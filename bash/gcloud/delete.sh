#!/usr/bin/env bash

# Delete gcloud resources
# Author: Dylan Hitt Date: Nov. 30th 2019

# set -o errexit
# set -o pipefail

### Deleting compute instances ###

echo "Deleting compute instances"

COMPUTE_LIST=$(gcloud compute instances list \
    --filter="tags.items=('kubernetes-the-hard-way')" \
    --format json)

echo $COMPUTE_LIST | jq -r -c ".[] | .name" | while read i; do 
    echo "Deleting ${i}"
    gcloud -q compute instances delete ${i} 
done 


### Deleting network and firewall rules ###

# # Delete public IP Address
# gcloud -q compute addresses delete kubernetes-the-hard-way

# # Delete firewall rules
# gcloud -q compute firewall-rules delete kubernetes-the-hard-way-allow-external
# gcloud -q compute firewall-rules delete kubernetes-the-hard-way-allow-internal

# # Delete subnet
# gcloud -q compute networks subnets delete kubernetes

# # Delete network
# gcloud -q compute networks delete kubernetes-the-hard-way 

