#!/usr/bin/env bash

# Setup compute instances for kuberrnetes cluster
# Configure firewall rules
# Author: Dylan Hitt Date: Nov. 30th 2019 

set -o errexit
set -o pipefail

cd "$(dirname "$0")"

CONTROLLERS_ARR_INDEX=$((${1:-3}-1))
WORKERS_ARR_INDEX=$((${2:-3}-1))

# Provision Controllers
for i in $(seq 0 ${CONTROLLERS_ARR_INDEX}); do
  gcloud compute instances create controller-${i} \
    --async \
    --boot-disk-size 200GB \
    --can-ip-forward \
    --image-family ubuntu-1804-lts \
    --image-project ubuntu-os-cloud \
    --machine-type n1-standard-1 \
    --private-network-ip 10.240.0.1${i} \
    --scopes compute-rw,storage-ro,service-management,service-control,logging-write,monitoring \
    --subnet kubernetes \
    --tags kubernetes-the-hard-way,controller
done

# Provision Workers
for i in $(seq 0 ${WORKERS_ARR_INDEX}); do
  gcloud compute instances create worker-${i} \
    --async \
    --boot-disk-size 200GB \
    --can-ip-forward \
    --image-family ubuntu-1804-lts \
    --image-project ubuntu-os-cloud \
    --machine-type n1-standard-1 \
    --metadata pod-cidr=10.200.${i}.0/24 \
    --private-network-ip 10.240.0.2${i} \
    --scopes compute-rw,storage-ro,service-management,service-control,logging-write,monitoring \
    --subnet kubernetes \
    --tags kubernetes-the-hard-way,worker
done

echo "Validating workers"

WORKERS=($(gcloud compute instances list \
    --filter="tags.items=('kubernetes-the-hard-way')" \
    --filter="tags.items=('worker')" \
    --format json | jq -r -c ".[] | .name"))

if [ ${#WORKERS[@]} -eq 3 ]; then 
	echo "Workers have been created"; 
fi

echo "Validating controllers"

CONTROLLERS=($(gcloud compute instances list \
    --filter="tags.items=('kubernetes-the-hard-way')" \
    --filter="tags.items=('controller')" \
    --format json | jq -r -c ".[] | .name"))

if [ ${#CONTROLLERS[@]} -eq 3 ]; then 
	echo "Workers have been created"; 
fi