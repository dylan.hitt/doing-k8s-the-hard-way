#!/usr/bin/env bash

# Setup VPC network for kuberrnetes cluster
# Configure firewall rules
# Author: Dylan Hitt Date: Nov. 30th 2019 

set -o errexit
set -o pipefail

cd "$(dirname "$0")"

# create the VPC network
echo "Creating the VPC network!"
gcloud compute networks create kubernetes-the-hard-way \
    --subnet-mode custom 
echo "Created the VPC network!"

# create subnet in the VPC network
echo "Creating the subnet in the VPC network!"
gcloud compute networks subnets create kubernetes \
  --network kubernetes-the-hard-way \
  --range 10.240.0.0/24
echo "Created the subnet in the VPC network!"

# create firewall rules that allow internal communication
echo "Creating internal firewall rules!"
gcloud compute firewall-rules create kubernetes-the-hard-way-allow-internal \
  --allow tcp,udp,icmp \
  --network kubernetes-the-hard-way \
  --source-ranges 10.240.0.0/24,10.200.0.0/16
echo "Created internal firewall rules!"

# create firewall rules that allow external communication
echo "Creating external firewall rules!"
gcloud compute firewall-rules create kubernetes-the-hard-way-allow-external \
  --allow tcp:22,tcp:6443,icmp \
  --network kubernetes-the-hard-way \
  --source-ranges 0.0.0.0/0
echo "Created external firewall rules!"

readonly EXPECTED_RULES="tcp22tcp6443icmptcpudpicmp"
RULESOUTPUT_ARRAY=$(gcloud compute firewall-rules list \
    --filter="network:kubernetes-the-hard-way" \
    --format=json | jq -r '.[] | .allowed[] | "\(.IPProtocol)\(.ports[]? //"")"')   

source ../utils.sh

TRIMMED_OUTPUT=$(_trim "${RULESOUTPUT_ARRAY[@]}")

if [ "${EXPECTED_RULES}" == "${TRIMMED_OUTPUT}" ]; then 
    echo "Firewall rules verified"
else 
    echo "Firewall rules error!"
fi

gcloud compute addresses create kubernetes-the-hard-way \
  --region $(gcloud config get-value compute/region)

STATUS=$(gcloud compute addresses list \
    --filter="name=('kubernetes-the-hard-way')" \
    --format=json | jq -r '.[] | .status')  

if [ "${STATUS}" == "RESERVED" ]; then 
	echo "Public IP address created"
else 
	echo "IP creation error!"
fi