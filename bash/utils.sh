#!/usr/bin/env bash

set -o errexit
set -o pipefail

_trim() {
    local STRING_TO_TRIM=${1}
    local TRIMMED_STRING="$(echo "${STRING_TO_TRIM}" | tr -d '[:space:]')"
    echo "${TRIMMED_STRING}"
}

_get_public_ip_address() {
    local IP=$(gcloud compute addresses list \
        --filter="name=('kubernetes-the-hard-way')" \
        --format=json | jq -r '.[] | .address')
    echo "${IP}"
}