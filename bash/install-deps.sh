#!/usr/bin/env bash

# Shell script used to install kubectl client 
# and pki infrastructure used for tls
# Author: Dylan Hitt Date: Nov. 30th 2019

set -o errexit
set -o pipefail

cd "$(dirname "$0")"

# Checks to see if the tool already exists if so nothing, if not install it
_install_dependencies() {
    if ! type -P $1 > /dev/null; then 
        curl -o $1 $2
        chmod +x $1
        sudo mv $1 /usr/local/bin/
        echo "Installed ${1} to path"
    else
        echo "${1} is installed!"
        sleep 1
    fi
}

_install_dependencies cfssl https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/darwin/cfssl
_install_dependencies cfssljson https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/darwin/cfssljson
_install_dependencies kubectl https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/darwin/amd64/kubectl
# install_dependencies jq https://github.com/stedolan/jq/releases/download/jq-1.4/jq-osx-x86_64

brew install jq
echo "jq installed or already installed"







